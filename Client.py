'''
Andrew Smart
John C. Campbell
Apr 22, 2017
Computer Networks A365
'''

from tkinter import *
from tkinter import messagebox
from PIL import Image, ImageTk
from RtpPacket import RtpPacket
import socket, threading, sys, traceback, os

CACHE_FILE_NAME = "cache-"
CACHE_FILE_EXT = ".jpg"

class Client:
	INIT = 0
	READY = 1
	PLAYING = 2
	state = INIT

	SETUP = 0
	PLAY = 1
	PAUSE = 2
	TEARDOWN = 3

	counter = 0

	# socket, server, file info passed in from ClientLauncher
	def __init__(self, master, serveraddr, serverport, rtpport, filename):
		self.master = master
		self.master.protocol("WM_DELETE_WINDOW", self.handler)
		self.createWidgets()
		self.serverAddr = serveraddr
		self.serverPort = int(serverport)
		self.rtpPort = int(rtpport)
		self.fileName = filename
		self.rtspSeq = 0
		self.sessionId = 0
		self.requestSent = -1
		self.teardownAcked = 0
		self.connectToServer()
		self.frameNbr = 0
		self.rtpSocket = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

	# make the GUI with tkinter
	# not great but OK
	def createWidgets(self):
		# setup button
		self.setup = Button(self.master, width=20, padx=3, pady=3)
		self.setup["text"] = "Setup"
		self.setup["command"] = self.setupMovie
		self.setup.grid(row=1, column=0, sticky=S, padx=2, pady=2)
		# sticky not working

		# play button
		self.start = Button(self.master, width=20, padx=3, pady=3)
		self.start["text"] = "Play"
		self.start["command"] = self.playMovie
		self.start.grid(row=1, column=1, sticky=S, padx=2, pady=2)		

		# pause button
		self.pause = Button(self.master, width=20, padx=3, pady=3)
		self.pause["text"] = "Pause"
		self.pause["command"] = self.pauseMovie
		self.pause.grid(row=1, column=2, sticky=S, padx=2, pady=2)		

		# teardown button
		self.teardown = Button(self.master, width=20, padx=3, pady=3)
		self.teardown["text"] = "Teardown"
		self.teardown["command"] =  self.exitClient
		self.teardown.grid(row=1, column=3, sticky=S, padx=2, pady=2)		

		# make a label to display the movie
		self.label = Label(self.master, height=39)
		self.label.grid(row=0, column=0, columnspan=4, sticky=W+E+N+S, padx=5, pady=5)		

	# start button handler
	def setupMovie(self):
		if self.state == self.INIT:
			self.sendRtspRequest(self.SETUP)

	# clear out gui at the end
	def exitClient(self):
		# send out closing request to server
		self.sendRtspRequest(self.TEARDOWN)
		# kill the gui window
		self.master.destroy() 
		try:
			# delete the cache image from the video
			os.remove(CACHE_FILE_NAME + str(self.sessionId) + CACHE_FILE_EXT) 
		except:
			print("No temporary image files left to remove")
		if self.frameNbr > 0:
			rate = float(self.counter/self.frameNbr)
		else:
			rate = 0
		print ("RTP Packet Loss Rate :" + str(rate))
		sys.exit(0)

	# pause button handler
	def pauseMovie(self):
		if self.state == self.PLAYING:
			self.sendRtspRequest(self.PAUSE)

	# play button handler
	def playMovie(self):
		if self.state == self.READY:
			# new thread to listen for packets
			print ("Playing Movie")
			threading.Thread(target=self.listenRtp).start()
			self.playEvent = threading.Event()
			self.playEvent.clear()
			self.sendRtspRequest(self.PLAY)

	# get packets from server loop
	def listenRtp(self):
		while True:
			try:
				data,addr = self.rtpSocket.recvfrom(20480)
				# if received a packet
				if data:
					rtpPacket = RtpPacket()
					rtpPacket.decode(data)
					print ("Received Rtp Packet #" + str(rtpPacket.seqNum()))

					try:
						if self.frameNbr > 254: self.frameNbr = -1
						if self.frameNbr + 1 != rtpPacket.seqNum():
							self.counter += 1
							print ("PACKET LOSS")
						currFrameNbr = rtpPacket.seqNum()
						#version = rtpPacket.version()
					except:
						print ("ERROR: seqNum() problem in Client/listenRtp")
						traceback.print_exc(file=sys.stdout)

					# handle late or delayed packets
					if currFrameNbr > self.frameNbr:
						self.frameNbr = currFrameNbr
						self.updateMovie(self.writeFrame(rtpPacket.getPayload()))

			except:
				# failure or got a request of PAUSE or TEARDOWN
				print ("Didn`t receive data!")
				if self.playEvent.isSet():
					break

				# received ACK for TEARDOWN request,
				# close the socket
				if self.teardownAcked == 1:
					try:
						self.rtpSocket.shutdown(socket.SHUT_RDWR)
						self.rtpSocket.close()
						break
					except:
						print("Socket already closed")

	# make a temporary jpeg file for the frame
	def writeFrame(self, data):

		# make filename
		cachename = CACHE_FILE_NAME + str(self.sessionId) + CACHE_FILE_EXT
		try:
			# make file
			file = open(cachename, "wb")
		except:
			print ("ERROR: File open issue in Client/writeFrame")
		try:
			# write the jpeg
			file.write(data)
		except:
			print ("ERROR: write issue in Client/writeFrame")
		file.close()
		return cachename

	# show the jpeg in the GUI label
	def updateMovie(self, imageFile):
		try:
			# fixed
			photo = ImageTk.PhotoImage(Image.open(imageFile))
		except:
			print ("ERROR: photo problem in Client/updateMovie")
			traceback.print_exc(file=sys.stdout)

		self.label.configure(image = photo, height=288)
		self.label.image = photo

	# start session with server
	def connectToServer(self):
		self.rtspSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			self.rtspSocket.connect((self.serverAddr, self.serverPort))
		except:
			messagebox.showwarning('Connection Failed', 'Connection to \'%s\' failed.' %self.serverAddr)

	# send request to server, uses state and buttons
	def sendRtspRequest(self, requestCode):
		"""Send RTSP request to the server."""

		# if were're starting
		if requestCode == self.SETUP and self.state == self.INIT:
			threading.Thread(target=self.recvRtspReply).start()
			self.rtspSeq = 1
			# make request
			request = "SETUP " + str(self.fileName) + "\n" + str(self.rtspSeq) + "\n" + " RTSP/1.0 RTP/UDP " + str(self.rtpPort)
			request = request.encode()
			self.rtspSocket.send(request)
			# store the request
			self.requestSent = self.SETUP

		# play/resume request
		elif requestCode == self.PLAY and self.state == self.READY:
			self.rtspSeq = self.rtspSeq + 1
			# make request
			request = "PLAY " + "\n" + str(self.rtspSeq)
			request = request.encode()
			# send request
			self.rtspSocket.send(request)
			print ("PLAY request sent")
			# store the request
			self.requestSent = self.PLAY

		# pause request
		elif requestCode == self.PAUSE and self.state == self.PLAYING:

			self.rtspSeq = self.rtspSeq + 1
			# make request
			request = "PAUSE " + "\n" + str(self.rtspSeq)
			request = request.encode()
			# send request
			self.rtspSocket.send(request)
			print ("PAUSE request sent")
			# store the request
			self.requestSent = self.PAUSE

		# teardown request
		elif requestCode == self.TEARDOWN and not self.state == self.INIT:
			self.rtspSeq = self.rtspSeq + 1
			# make request
			request = "TEARDOWN " + "\n" + str(self.rtspSeq)
			request = request.encode()
			# send request
			self.rtspSocket.send(request)
			print ("TEARDOWN request sent")
			# store the request
			self.requestSent = self.TEARDOWN

		else:
			# do nothing
			return

	# got a reply from server
	def recvRtspReply(self):
		while True:
			reply = self.rtspSocket.recv(1024)

			if reply:
				self.parseRtspReply(reply)

			# close socket on teardown
			if self.requestSent == self.TEARDOWN:
				try:
					self.rtspSocket.shutdown(socket.SHUT_RDWR)
					self.rtspSocket.close()
					os._exit(1)
					break
				except:
					print("Socket already closed")
					os._exit(1)
					break
	
	# parse the reply
	def parseRtspReply(self, data):
		print ("Parsing Received Rtsp data...")
		data = data.decode()
		lines = data.split('\n')
		seqNum = int(lines[1].split(' ')[1])

		# parse if the reply sequence number = request sequence number
		if seqNum == self.rtspSeq:
			session = int(lines[2].split(' ')[1])
			# new session ID
			if self.sessionId == 0:
				self.sessionId = session

			# parse if the session ID is ==
			if self.sessionId == session:
				if int(lines[0].split(' ')[1]) == 200:
					if self.requestSent == self.SETUP:
						self.state = self.READY
						# open port
						print ("Setting Up RtpPort for Video Stream")
						self.openRtpPort()
					elif self.requestSent == self.PLAY:
						self.state = self.PLAYING
						print ('-'*60 + "\nClient is PLAYING...\n" + '-'*60)
					elif self.requestSent == self.PAUSE:
						self.state = self.READY
						# a play thread exits, create a new thread resume
						self.playEvent.set()

					elif self.requestSent == self.TEARDOWN:
						# acked the teardown request
						self.teardownAcked = 1

	# open a socket to the server
	def openRtpPort(self):
		self.rtpSocket.settimeout(1)
		try:
			# need rtpPort > 1024
			self.rtpSocket.bind((self.serverAddr,self.rtpPort))   
			print ("Bind RtpPort Success")
			self.updateMovie("ready.jpeg")
		except:
			messagebox.showwarning('Connection Failed', 'Connection to rtpServer failed...')

	# handle hitting the X close button on the window
	def handler(self):
		self.pauseMovie()
		if messagebox.askokcancel("Quit?", "Are you sure you want to quit?"):
			self.exitClient()
		else: 
			# on cancel, resume
			print ("Playing Movie")
			threading.Thread(target=self.listenRtp).start()
			self.sendRtspRequest(self.PLAY)
