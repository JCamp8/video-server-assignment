'''
Andrew Smart
John C. Campbell
Apr 22, 2017
Computer Networks A365
'''

import struct, time
from VideoConverter import VideoConverter

class VideoStream:

	# initalize
	def __init__(self, filename):

		VideoConverter(filename)
		time.sleep(5)
		self.filename = "outfile.mjpeg" # accessable everywhere
		
		try:
			self.file = open(self.filename, 'rb')
			print ("Video file : |" + self.filename +  "| read")
		except:
			print ("ERROR reading file in VideoStream/init")
			raise IOError
		self.frameNum = 0

	# return next jpeg of movie
	def nextFrame(self):

		location = -1
		startSpot = 0
		endSpot = 0
		try:
			byteLocation = self.file.read(1)
			while byteLocation != "":
				location = location + 1
				# this if checks where the chunks begin and end
				if int.from_bytes(byteLocation, byteorder='big') == int.from_bytes(b'\xFF', byteorder='big'):
					byteLocation = self.file.read(1)
					location = location + 1
					# this is the begin
					if int.from_bytes(byteLocation, byteorder='big') == int.from_bytes(b'\xD8', byteorder='big'):
						startSpot = location-2
					# this is the end
					if int.from_bytes(byteLocation, byteorder='big') == int.from_bytes(b'\xD9', byteorder='big'):
						endSpot = location
						break
				byteLocation = self.file.read(1)
		except:
			print("EOF")	

		if byteLocation:

			framelength = endSpot - startSpot
			startAt = self.file.tell() - (framelength) # start of the chunk
			self.file.seek(startAt) # set position of file pointer
			frame = self.file.read(framelength)
			if len(frame) != framelength:
				raise ValueError("ERROR: bad frame length in VideoStream/nextFrame")
			
			# wrap frame numbers since becomes seqnum
			# temp = self.frameNbr
			if int(self.frameNum) > 255:
				self.frameNum = 0
			else:
				self.frameNum += 1
			print ("Next Frame (#" + str(self.frameNum) + ") length:" + str(framelength) + "")

			return frame

	# returns frame number to caller, keeps track of where we are
	def frameNbr(self):

		return self.frameNum