'''
Andrew Smart
John C. Campbell
Apr 22, 2017
Computer Networks A365
'''

import random, math, time
import sys, traceback, threading, socket
from VideoStream import VideoStream
from VideoConverter import VideoConverter
from RtpPacket import RtpPacket
from random import randint

class ServerWorker:
	SETUP = 'SETUP'
	PLAY = 'PLAY'
	PAUSE = 'PAUSE'
	TEARDOWN = 'TEARDOWN'

	INIT = 0
	READY = 1
	PLAYING = 2
	state = INIT

	OK_200 = 0
	FILE_NOT_FOUND_404 = 1
	CON_ERR_500 = 2

	clientInfo = {}

	def __init__(self, clientInfo):
		self.clientInfo = clientInfo

	def run(self):
		threading.Thread(target=self.recvRtspRequest).start()

	# reply to client packet, make and send
	def replyRtsp(self, code, seq):
		if code == self.OK_200:
			reply = 'RTSP/1.0 200 OK\nCSeq: ' + seq + '\nSession: ' + str(self.clientInfo['session'])
			reply = reply.encode()
			connSocket = self.clientInfo['rtspSocket'][0]
			connSocket.send(reply)
		elif code == self.FILE_NOT_FOUND_404:
			print ("404 FILE NOT FOUND")
		elif code == self.CON_ERR_500:
			print ("500 CONNECTION ERROR")	

	# make a packet for sending
	def makeRtp(self, payload, frameNbr):
		
		version = 2
		padding = 0
		extension = 0
		cc = 0
		marker = 0
		pt = 26
		seqnum = frameNbr
		if seqnum > 255: seqnum = 0 # two bit seqnum, wrap
		ssrc = 0

		rtpPacket = RtpPacket()
		rtpPacket.encode(version, padding, extension, cc, seqnum, marker, pt, ssrc, payload)
		return rtpPacket.getPacket()	

	# get requests from client
	def recvRtspRequest(self):
		connSocket = self.clientInfo['rtspSocket'][0]
		while True:
			data = connSocket.recv(256)
			if data:
				# got data, process
				print ("Data received:")
				data = data.decode()
				self.processRtspRequest(data)

	# process data from client
	def processRtspRequest(self, data):
		# got request
		request = data.split('\n')
		line1 = request[0].split(' ')
		requestType = line1[0]
		# get file name
		filename = line1[1]
		# get sequence number
		seq = request[1].split(' ')

		# do SETUP request
		if requestType == self.SETUP:
			if self.state == self.INIT:

				print ("SETUP Request received\n")
				try:
					self.clientInfo['videoStream'] = VideoStream(filename)
					filename = "outfile.mjpeg"
					self.state = self.READY
				except IOError:
					self.replyRtsp(self.FILE_NOT_FOUND_404, seq[1])
				# make a random session ID
				self.clientInfo['session'] = randint(100000, 999999)
				# make reply
				#seq[0] = sequenceNum from client
				self.replyRtsp(self.OK_200, seq[0])
				print ("sequenceNum is " + seq[0])
				# set port from last line and send respnose
				self.clientInfo['rtpPort'] = request[2].split(' ')[3]
				print ("rtpPort is :" + self.clientInfo['rtpPort'])
				print ("filename is " + filename)

		# got a PLAY request
		elif requestType == self.PLAY:
			if self.state == self.READY:
				print ("PLAY Request Received")
				self.state = self.PLAYING
				# Create a new socket for sending out the data
				self.clientInfo["rtpSocket"] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
				self.replyRtsp(self.OK_200, seq[0])
				print ("Sequence Number ("+ seq[0] + ")\nReplied to client")
				# Create a new thread and start sending packets
				self.clientInfo['event'] = threading.Event()
				self.clientInfo['worker']= threading.Thread(target=self.sendRtp)
				self.clientInfo['worker'].start()
				
		# got a RESUME request
			elif self.state == self.PAUSE:
				print ("RESUME Request Received")
				self.state = self.PLAYING
				
		# got a PAUSE request
		elif requestType == self.PAUSE:
			if self.state == self.PLAYING:
				print ("PAUSE Request Received")
				self.state = self.READY
				self.clientInfo['event'].set()
				self.replyRtsp(self.OK_200, seq[0])

		# got a TEARDOWN request
		elif requestType == self.TEARDOWN:
			print ("TEARDOWN Request Received")
			self.clientInfo['event'].set()
			# send OK
			self.replyRtsp(self.OK_200, seq[0])
			# close socket
			self.clientInfo['rtpSocket'].close()
			# DEBUG: leave commented out to check if file created
			VideoConverter.killFile()

	# send the packet
	def sendRtp(self):
		counter = 0
		threshold = 10
		while True:
			# set up jitter/delay between packets
			# someday variable speed to accomidate different framerates
			# no clue how to get framerate info yet
			jit = math.floor(random.uniform(-13,5.99))
			jit = jit / 1000

			self.clientInfo['event'].wait(0.05 + jit)
			jit = jit + 0.020

			# stop on PAUSE or TEARDOWN
			if self.clientInfo['event'].isSet():
				break

			data = self.clientInfo['videoStream'].nextFrame()
			#print ("data from nextFrame():\n" + data)
			if data:
				frameNumber = self.clientInfo['videoStream'].frameNbr()
				try:
					# ya know, I got this code from somewhere and it works and it helps
					# 95% time does a short wait
					port = int(self.clientInfo['rtpPort'])
					prb = math.floor(random.uniform(1,100))
					if prb > 5.0:
						try:
							self.clientInfo['rtpSocket'].sendto(self.makeRtp(data, frameNumber),(self.clientInfo['rtspSocket'][1][0],port))
							counter += 1
							time.sleep(jit)
						except:
							print ("ERROR: connection problem in ServerWorker/sendRtp")
				except:
					print ("ERROR: connection problem in ServerWorker/sendRtp")
					traceback.print_exc(file=sys.stdout)




