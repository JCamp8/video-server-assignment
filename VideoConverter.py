'''
Andrew Smart
John C. Campbell
Apr 22, 2017
Computer Networks A365
'''

import subprocess, sys, os

class VideoConverter:

    # initalize
    def __init__(self, filename):
            
        # ffmpeg, we are limited to *nix machines with ffmpeg installed
        try:    
            cmd = ["ffmpeg", "-i", filename, "-vcodec", "mjpeg", "outfile.mjpeg"]
            proc = subprocess.Popen(cmd, env={'PATH': os.getenv('PATH')})
            #ffmpeg -i orion_1.mpg -vcodec mjpeg outvid.mjpeg
        except:
            print("Error in subprocess ffmpeg")

    def killFile():
        try:
            os.remove("outfile.mjpeg") # Delete the temporary video
        except:
            print("No temporary image files left to remove")