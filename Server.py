'''
Andrew Smart
John C. Campbell
Apr 22, 2017
Computer Networks A365
'''

import sys, socket, threading
from ServerWorker import ServerWorker

class Server:

	# ends program so we don't have to kill it by pid
	def killProgram(): 
		# kills the program
		os._exit(1) 

	def main(self):
		# ############################ DEBUG MEASURE ########################## #
		threading.Timer(450, self.killProgram).start() # 7.5 minutes to auto-end
		# ############################ END DEBUG ############################## # 
		try:
			SERVER_PORT = int(sys.argv[1])
		except:
			print ("[Usage: Server.py Server_port]\n")
		rtspSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		rtspSocket.bind(('', SERVER_PORT))
		print ("RTSP Listing incoming request...")
		rtspSocket.listen(5)

		# Receive client info (address,port) through RTSP/TCP session
		while True:
			clientInfo = {}
			clientInfo['rtspSocket'] = rtspSocket.accept() 

			ServerWorker(clientInfo).run()

# start program
if __name__ == "__main__":
	(Server()).main()


