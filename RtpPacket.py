'''
Andrew Smart
John C. Campbell
Apr 22, 2017
Computer Networks A365
'''

import sys, VideoStream
from time import time

HEADER_SIZE = 12

class RtpPacket:

	def __init__(self):
		self.header = bytearray(HEADER_SIZE)
		
	# setup packer to send
	def encode(self, version, padding, extension, cc, seqnum, marker, pt, ssrc, payload):

		timestamp = int(time())
		print ("timestamp: " + str(timestamp))
		self.header = bytearray(HEADER_SIZE)

		# make bytearray with RTP header fields
		# version set to 2
		# all others 0
		# header length 12 bytes

		#header[0] = version + padding + extension + cc + seqnum + marker + pt + ssrc
		self.header[0] = version << 6
		self.header[0] = self.header[0] | padding << 5
		self.header[0] = self.header[0] | extension << 4
		self.header[0] = self.header[0] | cc
		self.header[1] = marker << 7
		self.header[1] = self.header[1] | pt
		
		if seqnum > 255: seqnum = 0 # wrap sequence numbers
		self.header[2] = seqnum >> 8
		self.header[3] = seqnum

		self.header[4] = (timestamp >> 24) & 0xFF
		self.header[5] = (timestamp >> 16) & 0xFF
		self.header[6] = (timestamp >> 8) & 0xFF
		self.header[7] = timestamp & 0xFF

		self.header[8] = ssrc >> 24
		self.header[9] = ssrc >> 16
		self.header[10] = ssrc >> 8
		self.header[11] = ssrc

		# Get the payload from the argument
		self.payload = payload

	#split packet into header & payload
	def decode(self, byteStream):

		#print (byteStream[:HEADER_SIZE])
		self.header = bytearray(byteStream[:HEADER_SIZE])
		self.payload = byteStream[HEADER_SIZE:]

	# version, not really used at this time
	def version(self):
		return int(self.header[0] >> 6)

	# sequence number, two bytes wraps at 255
	def seqNum(self):
		#header[2] shift left for 8 bits then bitwise or with header[3]
		seqNum = self.header[2] << 8 | self.header[3]
		return int(seqNum)

	# timestamp
	def timestamp(self):
		timestamp = self.header[4] << 24 | self.header[5] << 16 | self.header[6] << 8 | self.header[7]
		return int(timestamp)

	# type, not really used at this time
	def payloadType(self):
		pt = self.header[1] & 127
		return int(pt)

	# give video frame
	def getPayload(self):
		return self.payload

	# return built packet
	def getPacket(self):
		return self.header + self.payload