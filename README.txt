Andrew Smart
John C. Campbell
April 22, 2017
Computer Networks A365

README: 

Assignment:

This is the video streaming server assignment from "Computer Networking: A Top-Down Approach" by Kurose and Ross from Pearson publishing.

Credit:

All due credit to 'Tibbers' from Github who did this assignment in an earlier version of Python, probably 2.7 or something. Code has been updated to 3.4, extended to most video formats through ffmpeg, and no longer crashes after the first 255 frames.
All due credit to the creators of ffmpeg, I couldn't have gotten our assigned video, much less the extra credit, without them.
All due credit to Google which turned up Tibbers earlier version of the assignment.

Instructions:

The client is started with the command "ClientLauncher.py Server_address Server_port RTP_port Video_file". 
The server is started with "Server.py Server_port". 
Implementation is complete for video, there is no audio component, no framerate adjustment, and small scaling.
It requires ffmpeg, Python's PIL and tkinter packages and runs in python 3.4
Code works on linux machine but may not work on other machines due to ffmpeg command line options. 
Files that have been tested with are orion_1.mpg and skeet.wmv. Both are included in the submission. 
Was not tested on tikishla due to there being no installation of ffmpeg on the school server.
The stdout output is verbose and a great help in debugging, you're supposed to be watching the video anyways.

Example command lines:

python3 Server.py 9999
python3 ClientLauncher.py 127.0.0.1 9999 8888 skeet.wmv
python3 ClientLauncher.py 127.0.0.1 9999 8888 orion_1.mpg

Issues:

Andrew: Was completely unable to install PIL on my laptop. Everything that I tried failed. Spent a good few hours on it. Whether using the MS installer, going through command line or any other possible way I found issues and errors would arise, up to and including un and reinstalling python. These ranged from the Microsoft installer being unable to locate my installation of "python 3.6-32 in the register" to the command line saying that it is, "unable to create command using '"' ". Issue was looked at by others with same lack of progress. This meant my contribution to the code was limited, as I was unable to run most of the code without PIL.

John: 

It was nice to do most of the coding and not be stuck on solving a few intractable errors this time. Coding is fun, errors annoy. Sending and receiving was relatively straightforward but the video encoding was a complete bugger. I ended up looking at encoding schemes and examining video files with a hex editor to try to determine values. The mjpeg format turned out to be the easiest to implement and the final program uses the third party tool ffmpeg to convert any given video file into a temporary mjpeg file for transmission.

4 hours April 22nd
1) formatting and code cleanup
2) multiple file format testing and fixing

16 hours on April 21st
1) packet setup and transmission
2) headers and file formats
3) file conversions

9 to 12 hours spread across April 14th to 20th
1) searching for and installing components
2) finding templates to base code on
3) fixing templates and setting up
